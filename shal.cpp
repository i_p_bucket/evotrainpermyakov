void MainWindow::startGame
{
    new Game(ui.glWidget->renderer());
    if (!m_game->trySetup())
    {
        QMessage:critical(NULL, "Unable to start game", "Failed to launch the game");
        exitGame();
    }
    {
        m_game->initialize();
        m_game->run();
    }